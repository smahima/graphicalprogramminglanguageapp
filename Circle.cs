﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Circle, a more specific version of Shape, class inherited from it.
    /// Has some unique properties.
    /// </summary>
    class Circle :Shape
    {
        /// <summary>
        /// radius, a unique property of Circle, declared as  integer
        /// </summary>
        public int radius;

        /// <summary>
        ///Method overrides the set method from Shapes class.
        /// All the properties required for creating/drawing circle 
        /// all come in the order:color,fill status and radius
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(Color color, string fill ,params int[] list)
        {
            base.set(color,fill, list[0], list[1]); 
            this.radius = list[2];
        }

        /// <summary>
        ///Overriding the draw method from Shape class.
        /// Specifically draws a circle with the help of Graphics.DrawEllipse method
        /// uses the paramteres set from the set method
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(this.color, 2);
           
            g.DrawEllipse(p, xaxis-radius , yaxis-radius , this.radius * 2, this.radius * 2);
            
            if (this.fill == "true")
            {
                SolidBrush sb = new SolidBrush(this.color);

                g.FillEllipse(sb, xaxis-radius, yaxis-radius, radius*2, radius*2);
            }
        }

    }
}
