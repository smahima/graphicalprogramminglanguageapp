﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// A class that decodes each command provided by user.
    /// Checks if commands are valid,parameters are fine
    /// Finally recognizes each command as passes appropriate indication
    /// </summary>
    public class CommandParser

    {
        

        int errortype = 0;
        //ProgramBox.
        /// <summary>
        /// A method that deconstructs each command and parameters (if they have)
        /// </summary>
        /// <param name="line">The line of command coming from Commandline or Program Box</param>
        /// <returns>An integer array, indication which command it is and its parameters</returns>
        public int[] commandbreakdown(String line)
        {
            
            int commandtype;
            line = line.ToLower().Trim();
            string[] split = line.Split(' ');
            int length=split.Length;

            if(length>=3)
            {
                this.errortype = 1;
                commandtype = 9;
                int[] arr = { commandtype  };
                return arr;
            }
            

            if (split.Length == 2)
            {
                string command = split[0];


                if (command == "fill")
                {   
                    if (split[1] == "on")
                    { commandtype = 7;
                        int[] arr = {commandtype, 1 };
                        return arr;
                    }

                    //return 
                    else if (split[1] == "off")
                    {
                        commandtype = 6;
                        int[] arr = {commandtype, 0 };
                        return arr; }
                    else
                    {
                        this.errortype = 1;
                        commandtype = 9;
                        int[] arr = { commandtype };
                        return arr;
                    }
                }
                else if(command =="pen")
                {
                    if (split[1]=="blue")
                    {
                       
                        commandtype = 8;
                       
                            int[] arr = { commandtype, 3 };
                        return arr;
                    }
                    else if (split[1] == "yellow")
                    {
                        commandtype = 8;
                            int[] arr = { commandtype, 4 };
                        return arr;
                    }
                    else if (split[1] == "black")
                    {
                        commandtype = 8;
                            int[] arr = { commandtype, 5 };
                        return arr;
                    }
                    else
                    {
                        this.errortype = 1;
                        commandtype = 9;
                        int[] arr = { commandtype };
                        return arr;
                    }
                }
                else if(command!="fill" && command!="pen" && command!="rectangle" && command!="circle" && command!="triangle" && command!="moveto" && command!="drawto")
                {

                    this.errortype = 2;
                    commandtype = 9;
                    int[] arr = { commandtype };
                    return arr;
                }
                else
                {
                    //now, parsing parameters
                    string[] parameters = split[1].Split(',');
                    
                    int[] actualparam = new int[parameters.Length];
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        try
                        {
                            actualparam[i] = int.Parse(parameters[i]);
                        }
                        catch (Exception )
                        {
                            this.errortype = 4;
                            commandtype = 9;
                            int[] arr = { commandtype };
                            return arr;
                        }
                    }

                    if (command == "drawto")
                    {
                        if (actualparam.Length > 2 || actualparam.Length < 2)
                        {
                            this.errortype = 3;
                            commandtype = 9;
                            int[] arr = { commandtype };
                            return arr;
                        }
                        else
                        {
                            commandtype = 2;
                            int[] arr = { commandtype, actualparam[0], actualparam[1] };
                            return arr;
                        }
                    }
                    else if (command == "moveto")
                    {
                        if (actualparam.Length > 2 || actualparam.Length < 2)
                        {
                            this.errortype = 3;
                            commandtype = 9;
                            int[] arr = { commandtype };
                            return arr;
                        }
                        else
                        {
                            commandtype = 3;
                            int[] arr = { commandtype, actualparam[0], actualparam[1] };
                            return arr;
                        }
                    }
                    else if (command == "rectangle")
                    {
                        if (actualparam.Length != 2)
                        {
                            this.errortype = 3;
                            commandtype = 9;
                            int[] arr = { commandtype };
                            return arr;

                        }
                        else
                        {
                            commandtype = 12;
                            int[] arr = { commandtype, actualparam[0], actualparam[1] };
                            return arr;
                        }
                    }
                    else if (command == "triangle")
                    {
                        if (actualparam.Length != 3)
                        {
                            this.errortype = 3;
                            commandtype = 9;
                            int[] arr = { commandtype };
                            return arr;

                        }
                        {
                            commandtype = 13;
                            int[] arr = { commandtype, actualparam[0], actualparam[1], actualparam[2]};
                            return arr;
                        }
                    }
                    else if (command == "circle")
                    {
                        if (actualparam.Length != 1)
                        {
                            this.errortype = 3;
                            commandtype = 9;
                            int[] arr = { commandtype };
                            return arr;

                        }
                        else
                        {
                            commandtype = 1;
                            int[] arr = { commandtype, actualparam[0] };
                            return arr;
                        }
                    }
                }

                return null;
            }
            if (split.Length == 1)
            {
                string command = split[0];
                if (command == "clear")
                {
                    commandtype = 4;
                    int[] arr = { commandtype };
                    return arr;
                }
                else if (command == "reset") 
                {
                    commandtype = 5;
                    int[] arr = { commandtype };
                    return arr;
                }
                else if(command=="run")
                {
                    commandtype = 10;
                    int[] arr = { commandtype };
                    return arr;
                }
                else
                {
                    commandtype = 9;
                    this.errortype = 2;
                    int[] arr = { commandtype };
                    return arr;
                }



            }

            return null;
        }

        /// <summary>
        /// A method that identifies the type of error and returns appropriate message
        /// </summary>
        /// <returns>a string to be shown on the screen</returns>
        public string showMessage()
        {
            if(this.errortype==1)
            {
                return "Your command is wrong ";
            }
            else if (this.errortype == 2)
            {
                return "Unknown command ";
            }
            else if(this.errortype==3)
            {
                return "Invalid number of parameters ";
            }
            else if(this.errortype==4)
            {
                return "Given parameters are not Integers ";
            }
            return "Nothing";
        }

       

        
    }
}
