﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Documents;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// The main class which responds and controls all the form events.
    /// This class takes input(commands) and gives output(shows appropriate result on picturebox)
    /// </summary>
    public partial class Form1 : Form
    {


        /**
         * Many variables are declared here. All of them are responsible for keeping track of something.
         * For eg; the fillstatus exists to check if there are commands to fill the shape.
         * The (errorstatus) keeps track if an error in input command has occured.
         * The (linenumber) keeps track of the line number of the program snippet
         * the value of (commandtype) recognizes what sort of command the user has given
         * colorcode keeps track of the color of the pen.
         * fill has string value true or false such that when drawing,there is a if statement to act accordingly.
         */
        int xaxis = 0;
        int yaxis = 0;
        int fillstatus = 0;
        int errorstatus = 0;
        int linenumber;
        int colorcode;
        int commandtype;
        int syntaxerror = 0;
        int part2linenumber = 0;
        int looptime = 0;

        string fill;
        String line;
        Color defaultcolor = Color.Green;

        bool ifHasLoop;
        bool loopHasIf;
        bool ifconditionMatch;

        CommandParser commandParser = new CommandParser();
        ShapeFactory newsf = new ShapeFactory();
        Shape newshape;


        public int whichPart = 1;

        String[] variable = { "radius", "width", "height", "repeat", "value", "breadth", "perpendicular", "theight", "value1", "value2" ,"randomValue"};
        String[] shapes = { "circle", "rectangle", "triangle", "drawto", "moveto" };

        /**
         * There are many arraylists used for second part,some store values, some variables and some commands
         */
        ArrayList variablelist = new ArrayList();
        ArrayList variablevaluelist = new ArrayList();
        ArrayList storedCommands = new ArrayList(); //for simple method
        ArrayList storedPCommands = new ArrayList();

        
        int containsif = 0;
        int containsmethod = 0;
        int containsloop = 0;

        /**
         * These variables keep track of the line numbers
         */
        int endiflinenumber = 0;
        int endlooplinenumber = 0;
        int looplineNumber = 0;
        int iflineNumber = 0;

        /// <summary>
        /// The entry point of the form.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// An essential method to check which part is being executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Part 1")
            {  button1.Text= "Part 2";
                this.whichPart = 2;
            }
            else if (button1.Text=="Part 2")
            {
                button1.Text = "Part 1";
                this.whichPart = 1;
            }

        }

        /// <summary>
        /// A method that is called automatically when a key is pressed in the command line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">specifies the key pressed by user</param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                
                if (whichPart==1)//checks which part need to be executed
                {
                    /*
                  * Every time enter key is pressed, a lot of things need to be considered
                  * Making sure errorstatus, linenumber is zero.
                  * Clear Message area, send the command from command line to decode.
                  */
                    errorstatus = 0;
                    linenumber = 0;
                    this.line = commandLine.Text;
                    int[] parameterarray;
                    parameterarray = commandParser.commandbreakdown(line); 
                    messageArea.Clear();
                    
                    commandAction(parameterarray);
                }
                else if(whichPart==2)
                {
                    if (this.commandLine.Text == "run")
                    {
                        NewValidation nvn = new NewValidation();//an instance of new validation is created to check if loop contanis if and vice versa
                        nvn.ProgramCommands(ProgramBox);
                        variablelist.Clear();
                        variablevaluelist.Clear();
                        storedCommands.Clear();
                        storedPCommands.Clear();
                        messageArea.Clear();
                        ifHasLoop = nvn.ifhasLoop;
                        loopHasIf = nvn.LoophasIf;

                        if (nvn.isSomethingInvalid == false) //when the entire program passess validity check, the commands are then sent for execution
                        {

                            readCommands(); //first reads the commands for part 2
                        }
                        else
                        {
                            messageArea.Clear();
                            messageArea.AppendText("Your syntax is incorrect, cannot execute commands");
                        }
                    }
                }
            }
            
        }

        /// <summary>
        /// The DrawLine method draws a line from point of origin to a specified point
        /// </summary>
        /// <param name="p1">The xaxis of the point.</param>
        /// <param name="p2">The yaxis of the point</param>
        public void DrawLine(int p1, int p2)
        {
            Pen p = new Pen(defaultcolor, 2);
            Graphics g = OutputWindow.CreateGraphics();
            g.DrawLine(p, xaxis, yaxis, p1, p2);
            this.xaxis =  p1;
            this.yaxis =  p2;
            p.Dispose();
        }

        /// <summary>
        /// This method clears all the contents of the screen
        /// </summary>
        public void ClearScreen()
        {
            Graphics g = OutputWindow.CreateGraphics();
            g.Clear(Color.SkyBlue);
        }

        /// <summary>
        /// Method that displays appropriate error messages in the message display area
        /// </summary>
        /// <param name="message">The  message to display </param>
        public void DisplayText(String message)
        {
            if (linenumber != 0)
            {
                //message = message + "at line number:" + linenumber + ":PROGRAM HALTED!";
            }
           
            messageArea.AppendText(message);
            
            messageArea.AppendText(Environment.NewLine);
        }

        /// <summary>
        /// An important method that performs action according to the identified command.
        /// </summary>
        /// <param name="parameterarray">An integer array containing commandtype and parameters</param>
        public void commandAction(int[] parameterarray)
        {
            
            /*
             * Through this method, each action corresponding to command is taken.
             * Before each command action is taken, it is checked if it is being asked by program,
             * from the program block if so, no error should have occured so far
             */
            commandtype = parameterarray[0];
           
            if (commandtype == 2) //Represents drawto Method
            {
                int toX = parameterarray[1];
                int toY = parameterarray[2];
              
              
                if (linenumber==0)
                {
                    DrawLine(toX, toY);
                }
                else if(linenumber!=0 && errorstatus==0)
                {
                   
                    DrawLine(toX, toY);
                }
            }
            else if (commandtype == 3) //Represents moveto method
            {
                if (linenumber == 0)
                {
                    this.xaxis = parameterarray[1];
                    this.yaxis = parameterarray[2];
                }
                else if (linenumber != 0 && errorstatus == 0) 
                {    
                    this.xaxis = parameterarray[1];
                    this.yaxis = parameterarray[2];
                }
             }
            else if (commandtype == 4) //Represents clear method
            {
                    ClearScreen();
            }
            else if (commandtype == 5)//Represents reset method
            {
                if (linenumber == 0)
                {
                    this.xaxis = 0;
                    this.yaxis = 0;
                    this.defaultcolor = Color.Green;
                    fillstatus = 0;
                    
                }
                else if (linenumber != 0 && errorstatus == 0)
                {
                    this.xaxis = 0;
                    this.yaxis = 0;
                    this.defaultcolor = Color.Green;
                    fillstatus = 0;
                }
            }
            else if (commandtype == 7)
            {
                if (linenumber == 0)
                {
                    fillstatus = 1;
                }
                else if (linenumber != 0 && errorstatus == 0)
                {
                    fillstatus = 1;
                }
            }
            else if (commandtype == 6)//Represents fill status
            {
                if (linenumber == 0)
                {
                    fillstatus = 0;
                    
                }
                else if (linenumber != 0 && errorstatus == 0)
                {
                    fillstatus = 0;
                }
            }
            else if (commandtype == 8)//Represents color codes
            {
                colorcode = parameterarray[1];

                if (linenumber == 0)
                {
                    if (colorcode == 3)
                    {
                        this.defaultcolor = Color.Blue;

                    }
                    else if (colorcode == 4)
                    {
                        this.defaultcolor = Color.Yellow;
                    }
                    else
                    {
                        this.defaultcolor = Color.Black;
                    }
                }
                else if (linenumber != 0 && errorstatus == 0)
                {
                    if (colorcode == 3)
                    {
                        this.defaultcolor = Color.Blue;

                    }
                    else if (colorcode == 4)
                    {
                        this.defaultcolor = Color.Yellow;
                    }
                    else
                    {
                        this.defaultcolor = Color.Black;
                    }
                }

              
            }
            else if (commandtype == 1 || commandtype ==12 || commandtype==13) //The shape commands
            {

                line = line.ToLower().Trim();
                /**
                 * Another major code block, once commandtype indicates it belongs to creating shapes,
                 * this code is executed. First it checks if this command has come from direct command line
                 * or from porgram box. If line from command box is 'run', it implies the programbox is 
                 * asking to create shape. hence we again specifically change the value of line to a shape name
                 * with the help of command such that the application can understand which shape to draw.
                 */
                if (line =="run") 
                {
                   if(commandtype==1)
                    {
                        line = "circle";
                    }
                   else if (commandtype ==12)
                     {
                        line = "rectangle";
                    }
                   else 
                    {
                        line = "triangle";
                    }
                }

                /**
                 * Since all shapes when they need to be created come in this block, we should
                 * clean and parse the line's value Just in case it has come from command line directly.
                 * then it would mean the command would be something like: rect 20,30,which needs cleaning parsing
                 * to know what shape is it, rest parameters come from the parameterarray(from key down event)
                 */
                 string[] split = line.Split(' ');

                Shape s;
                ShapeFactory shapefactory = new ShapeFactory();

                /*
                 * Using Shape Factory to create shape
                 * Creating another array:arraystosend where all the values from parameterarray is transferred
                 * Also adding the axis points, maintaining order as they are sent.
                  */
                s = shapefactory.createShape(split[0]); 
                int[] arraystosend = new int[parameterarray.Length + 1];

                //the first two param are for orgigin
                arraystosend[0] = this.xaxis;
                arraystosend[1] = this.yaxis;
                int k = arraystosend.Length - 2;

                for (int i = 0; i < k; i++)
                {
                    arraystosend[i + 2] = parameterarray[i + 1]; //parameter[0] is type of command
                }

                if (fillstatus == 1)
                {
                    this.fill = "true"; 
                    s.set(defaultcolor, this.fill, arraystosend);
                }
                else
                {
                    s.set(defaultcolor, "false", arraystosend);
                }

                if (linenumber == 0) {
                    Graphics g = OutputWindow.CreateGraphics();
                    s.draw(g);
                }
                else if(linenumber != 0 && errorstatus == 0)
                {
                    Graphics g = OutputWindow.CreateGraphics();
                    s.draw(g);
                }
                 
                /**
                 * It is important to change the value of line to 'run' again such that, when in a loop(from program)
                 * the name of the shape to be created is changed according to the command type(again).
                 * Otherwisw same shape will be drawn continiously
                 */
                line = "run";
               
                /**
                 * Changing the origin once a shape is created
                 */
                if (split[0] == "circle")
                {
                    double tosubstract = arraystosend[2] / 3;

                    this.xaxis = this.xaxis + arraystosend[2]- (int)tosubstract;
                    this.yaxis = this.yaxis + arraystosend[2]- (int)tosubstract;
                   
                }
                else if (split[0] == "triangle")
                {
                    this.xaxis = arraystosend[2];
                    this.yaxis = arraystosend[4];
                }
                else
                {
                    this.xaxis = this.xaxis + arraystosend[2];
                    this.yaxis = this.yaxis + arraystosend[3];
                }
            }

            else if (commandtype == 9) //Command that recognizes error
            {
                string message = commandParser.showMessage();
                errorstatus = 1;
                if(linenumber!=0)//later check if this if block can be removed
                {   
                    message = message + "at line number:"+linenumber+":PROGRAM HALTED!";
                }

                
                DisplayText(message);
             }

            else if (commandtype == 10) //Command that recognizes run command
            {
                /**
                 * Everytime cli has returned run command, clear error status
                 * such that when next program is run, the previous error is forgotten
                 * or infact every time enter is pressed(which is done to execute a program) clear errorstatus
                 */

                errorstatus = 0;
                    if (ProgramBox.TextLength == 0)
                    {
                        string message = "No program to execute!";
                        DisplayText(message);
                    }//need another else if to run the checksyntax method
                     else
                      {
                       // Checksyntax();
                    
                        if(syntaxerror==0)
                        {
                        //inside the program

                        string program = ProgramBox.Text;

                        string[] split = program.Split('\n'); //Split each line
                        int[] var;

                        for (int i = 0; i < split.Length; i++) //loop until the number of lines in program
                        {
                            
                            linenumber = i + 1;
                            var = commandParser.commandbreakdown(split[i]);

                            /*
                             * Important to check if there is run command in program box, if it so
                             * the program should stop executing else,
                             * it will throw StackOverflowException
                             */

                            if (var[0] != 10)
                            {
                                commandAction(var); //Recursion applied here because ultimately this is the method
                            }                       //that does all the action.
                            else
                            {
                                errorstatus = 1;
                                DisplayText("Do not keep run command in program! Program invalid");
                            }

                        }//end of for loop!
                    }

                       }
                    
            }

        }

        /// <summary>
        /// This private method saves a program written in the Program Box to D: drive
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The event that is triggered when the menu item is clicked </param>
        private void saveProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if(ProgramBox.Text=="")
            {
                DisplayText("There is no program in the Text Area");
            }
            else
            {
                File.WriteAllText("D:\\program.txt",ProgramBox.Text);
                DisplayText("File Saved");
            }
        }

        /// <summary>
        /// This private method opens a File Dialog box and prompts user to choose a program text file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The event that is triggered when the menu item is clicked</param>
        private void loadProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
          
            String line="";
            openFileDialog.Filter = "Text files (.txt)| *.txt";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog.FileName);
                
                while(line !=null)
                {
                    line = sr.ReadLine();
                    if (line == null) break;
                    ProgramBox.Text += line;
                    
                    ProgramBox.AppendText(Environment.NewLine);
                }
            }
        }

        /// <summary>
        /// Exists the program when clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       

        /// <summary>
        /// This method is for the second part. In this method, each commands from the program box is parsed and action is taken accordingly.
        /// </summary>
        public void readCommands()
        {

            string commandlines = ProgramBox.Text;
            string[] eachline = commandlines.Split('\n');


            for (int i = 0; i < eachline.Length; i++)
            {
                part2linenumber = i + 1;
                eachline[i] = Regex.Replace(eachline[i], @"\s+", " ");
                string[] words = eachline[i].Split(' ');
                
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim(); //trim every command
                 }



                if (words[0] == "if") //from the program box, when if command is recognized
                {
                    this.containsif = 1;
                    int no;
                    int newindex;
                    int comparingEntity;
                    int givenentity;
                    if (int.TryParse(words[1], out no)) //first checking if the LHS or RHS is a variable
                    {

                        newindex = variablelist.IndexOf(words[3]); //this is the variable 
                        comparingEntity = Convert.ToInt32(variablevaluelist[newindex]);
                        givenentity = Convert.ToInt32(words[1]);
                    }
                    else
                    {
                        newindex = variablelist.IndexOf(words[1]); //this is the variable 
                        comparingEntity = Convert.ToInt32(variablevaluelist[newindex]);     //value of variable
                        givenentity = Convert.ToInt32(words[3]);
                    }
                    if (eachline[i].Contains('=')) //the line in which if was found, contains = to sign then,
                    {

                        if (comparingEntity == givenentity) //if the given condition satisfies
                        {
                            ifconditionMatch = true;
                            
                            if (!loopHasIf)//do not execute while if is inside loop
                            {
                                checkifstatement();
                            }
                        }
                    }
                    else if (eachline[i].Contains('>')) //the line in which if was found, contains > to sign then,
                    {
                        if (comparingEntity > givenentity) //if the given condition satisfies
                        {
                            ifconditionMatch = true;
                            if (!loopHasIf)//do not execute while if is inside loop
                            {
                                checkifstatement();
                            }
                        }
                    }
                    else if (eachline[i].Contains('<')) //the line in which if was found, contains < to sign then,
                    {
                        if (comparingEntity < givenentity) //if the given condition satisfies
                        {
                            ifconditionMatch = true;
                            if (!loopHasIf)//do not execute while if is inside loop
                            {
                                checkifstatement();
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("Given condition is not valid!");
                    }


                }

               else if (words[0] == "loop") //when loop is found
                {
                    this.containsloop = 1;
                    bool isInt = words[1].All(char.IsDigit);
                    if(isInt)
                    {
                        looptime = Int32.Parse(words[1]);
                    }
                    else
                    {
                        int index = variablelist.IndexOf("repeat"); //if repeat keyword exists, find it value
                        looptime = Convert.ToInt32(variablevaluelist[index]);
                    }

                    if (!ifHasLoop ) //donot execute any loop if loop block is inside if block
                    {
                        loopCommand(looptime);
                    }
                }

                else if ((words[0] == "method") ) //when method keyword is found
                {
                    this.containsmethod = 1;
                    if (words.Length > 1)
                    {
                        if (words[1] == "mymethod")
                        {
                            
                            methodCommand();//this method stores all the commands inside simple method in an arraylist
                        }
                        else if (words[1] == "mypmethod(param")
                        {
                            
                            if (words.Length < 4)
                            {
                                MessageBox.Show("Your parameterized method definition is incorrect. It should be:\n method mypmethod (param int[] list) ");
                            }
                            else
                            {
                                PmethodCommand();//this method stores all the commands inside parameterized method in an arraylist
                            }
                        }
                        else
                        {
                            MessageBox.Show("Unknown method specified!");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                            message: "Invalid method definition"
                            );
                    } 
                }
                
                
                else if (words[0]== "endmethod" || words[0]=="endpmethod")
                {

                    this.containsmethod = 0;
                }

                


                else if(words[0]=="endif")
                {
                    this.containsif = 0;
                }
                else if (words[0] == "endloop")
                {
                    this.containsloop = 0;
                }

                else if(words[0]=="call")
                {
                    
                    if(words.Length <= 1)
                    {
                        MessageBox.Show("Method call is invalid at line number:" + part2linenumber);
                    }
                    else
                    {
                        if(words[1]=="mymethod") //calling simple method
                        {
                            executeMethodCommand(); //send simple method for execution
                        }
                        else //calling parametrized method
                        {
                            PexecuteMethodCommand(words[1]); //send the arguments along with the execution of parameterized method
                        }
                    }
                   
                }

                /*
                 * unless if command is ended with endif, and method is ended with endmethod dont perform any commands
                 * because these commands aren't needed, the if block and method block execute shape commands separately,
                 * similar case for loop too
                 */
                if ((this.containsif == 0) && (this.containsmethod==0) && (this.containsloop==0) ) { 
                    
                    if (shapes.Contains(words[0]))                  
                    {
                        Console.WriteLine("shape command from above");
                        shapescommand(eachline[i]);
                    }//shape command

                    else if (variable.Contains(words[0]))
                    {
                        
                        variableassignment(eachline[i]);
                    } //variable assignment
                } //for now method and ifs and loop dont exist in same program
                



                if (words[0] == "clear")
                {
                    ClearScreen();
                }
                if(words[0]=="reset")
                {
                    this.xaxis = 0;
                    this.yaxis = 0;
                    this.defaultcolor = Color.Green;
                }


            }
        }

        /// <summary>
        /// Can be understood as a sub method, a helping method for parsing commands where it first searchs for endloop
        /// and executes all commands inside loop 
        /// </summary>
        /// <param name="repeatime">the number of times commands will loop</param>
        public void loopCommand(int repeatime)
        {

            string commandlines = ProgramBox.Text;
            string[] eachline = commandlines.Split('\n');
            
            
            for (int i = 0; i < eachline.Length; i++) //all this for finding out the endloop
            {
                looplineNumber = i + 1;
                eachline[i] = Regex.Replace(eachline[i], @"\s+", " ");
                string[] words = eachline[i].Split(' ');
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                    if (words[j] == "endloop")
                    {
                        endlooplinenumber = looplineNumber;
                        
                    }
                }
            }

            
            for (int j = 0; j < repeatime; j++)//for all the commands that need to repeat,inside loop block
            {
                
                for (int k = part2linenumber + 1; k < endlooplinenumber; k++)//again go through each command line inside the loop/endloop
                {
                    
                    eachline[k-1] = Regex.Replace(eachline[k-1], @"\s+", " ");
                    
                    string[] words = eachline[k-1].Split(' ');


                    if(words[0].Trim().ToLower()=="if") //if loop contains if statement
                    {
                        int no;
                        int newindex;
                        int comparingEntity;
                        int givenentity;
                        if (int.TryParse(words[1], out no))
                        {

                            newindex = variablelist.IndexOf(words[3]); //first, find out which one is variable
                            comparingEntity = Convert.ToInt32(variablevaluelist[newindex]);
                            givenentity = Convert.ToInt32(words[1]);
                        }
                        else
                        {
                            newindex = variablelist.IndexOf(words[1]); //first, find out which one is variable
                            comparingEntity = Convert.ToInt32(variablevaluelist[newindex]);     //value of variable
                            givenentity = Convert.ToInt32(words[3]);
                        }

                       /**
                        * Inside the if block now, first check the condition
                        */

                        if (eachline[k-1].Contains('='))
                        {
                            if (comparingEntity == givenentity) //if the given condition satisfies
                            {
                                ifconditionMatch = true;
                               
                                    checkifstatement(); //from the loop block, again call the method that executes if statement
                                
                            }
                        }
                        else if (eachline[k-1].Contains('>'))
                        {
                            if (comparingEntity > givenentity) //if the given condition satisfies
                            {
                                ifconditionMatch = true;
                                
                                    checkifstatement(); //from the loop block, again call the method that executes if statement

                            }
                        }
                        else if (eachline[k-1].Contains('<'))
                        {
                            if (comparingEntity < givenentity) //if the given condition satisfies
                            {
                                ifconditionMatch = true;
                               
                                    checkifstatement(); //from the loop block, again call the method that executes if statement


                            }
                        }
                       

                    }
                    /**
                     * These are the shape commands inside the loop block they will execute if they are contained inside loop block and if loop block doesn't contain if block
                     */
                    else if (words[0].Trim().ToLower()=="circle" && loopHasIf==false)
                    {
                       
                        bool isInt;
                        int radius=0;
                        newshape = newsf.createShape("circle");
                        isInt=words[1].All(char.IsDigit);
                        if (isInt)
                        {
                            radius = Int32.Parse(words[1]);
                        }
                        else
                        {
                            int index = variablelist.IndexOf("radius");
                             radius = Convert.ToInt32(variablevaluelist[index]);
                        }

                        int[] arraytosend3 = { this.xaxis, this.yaxis, radius}; 



                        newshape.set(defaultcolor, "false",arraytosend3);
                        Graphics g = OutputWindow.CreateGraphics();
                        newshape.draw(g);
                        double tosubstract = radius / 3;

                        this.xaxis = this.xaxis + radius - (int)tosubstract;
                        this.yaxis = this.yaxis + radius - (int)tosubstract;

                    }
                    else if (words[0].Trim().ToLower() == "rectangle" && loopHasIf == false)
                    {
                        bool isInt;
                        int width = 0;
                        int height = 0;

                        newshape = newsf.createShape("rectangle");
                        string[] parameters = words[1].Split(',');
                        isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                        if (isInt)
                        {
                            try
                            { 
                            width = Int32.Parse(parameters[0]);
                            height = Int32.Parse(parameters[1]);
                            }
                             catch (Exception e)
                            {
                            MessageBox.Show("Do not keep space before parameters at line number:" + part2linenumber);
                             }
                    }
                        else
                        {
                            int index = variablelist.IndexOf("width");
                            int index2 = variablelist.IndexOf("height");
                            width = Convert.ToInt32(variablevaluelist[index]);
                            height = Convert.ToInt32(variablevaluelist[index2]);
                        }

                       

                        int[] arraytosend3 = { this.xaxis, this.yaxis, width,height };
                        newshape.set(defaultcolor, "false", arraytosend3);
                        Graphics g = OutputWindow.CreateGraphics();
                        newshape.draw(g);

                        this.xaxis = this.xaxis + width;
                        this.yaxis = this.yaxis + height;

                    }
                    else if (words[0].Trim().ToLower() == "triangle" && loopHasIf == false)
                    {
                        bool isInt;
                        int breadth = 0;
                        int theight = 0;
                        int perpendicular = 0;

                        newshape = newsf.createShape("triangle");
                        string[] parameters = words[1].Split(',');
                        isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit) && parameters[2].All(char.IsDigit));
                        if (isInt)
                        {

                            breadth = Int32.Parse(parameters[0]);
                            theight = Int32.Parse(parameters[1]);
                            perpendicular = Int32.Parse(parameters[2]);
                        }
                        else
                        {
                            int index = variablelist.IndexOf("breadth");
                            int index2 = variablelist.IndexOf("theight");
                            int index3 = variablelist.IndexOf("perpendicular");
                            breadth = Convert.ToInt32(variablevaluelist[index]);
                            theight = Convert.ToInt32(variablevaluelist[index2]);
                            perpendicular = Convert.ToInt32(variablevaluelist[index3]);
                        }


                        int[] arraytosend3 = { this.xaxis, this.yaxis,breadth,theight,perpendicular };
                        newshape.set(defaultcolor, "false", arraytosend3);
                        Graphics g = OutputWindow.CreateGraphics();
                        newshape.draw(g);

                        this.xaxis = breadth;
                        this.yaxis = perpendicular;


                    }
                    else if(words[0].Trim().ToLower() == "drawto" && loopHasIf == false)
                    {
                        bool isInt;
                        int point1 = 0;
                        int point2 = 0;
                        string[] parameters = words[1].Split(',');
                        isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                        if (isInt)
                        {

                            point1 = Int32.Parse(parameters[0]);
                            point2 = Int32.Parse(parameters[1]);
                        }
                        else
                        {
                            int index = variablelist.IndexOf("value1");
                            int index2 = variablelist.IndexOf("value2");
                            point1 = Convert.ToInt32(variablevaluelist[index]);
                            point2 = Convert.ToInt32(variablevaluelist[index2]);
                        }


                        DrawLine(point1, point2);
                    }
                    else if (words[0].Trim().ToLower() == "moveto" && loopHasIf == false)
                    {
                        bool isInt;
                        int point1 = 0;
                        int point2 = 0;
                        string[] parameters = words[1].Split(',');
                        isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                        if (isInt)
                        {

                            point1 = Int32.Parse(parameters[0]);
                            point2 = Int32.Parse(parameters[1]);
                        }
                        else
                        {
                            int index = variablelist.IndexOf("value1");
                            int index2 = variablelist.IndexOf("value2");
                            point1 = Convert.ToInt32(variablevaluelist[index]);
                            point2 = Convert.ToInt32(variablevaluelist[index2]);
                        }
                        this.xaxis = point1;
                        this.yaxis =point2;
                    }
                    else if (variable.Contains(words[0]))//for variable assignment inside loop
                    {
                        variableassignment(eachline[k-1]);
                    }
                    else if(words[0].Trim().ToLower()=="method")
                    {
                        MessageBox.Show("Method is not supported inside loop at line number:" + part2linenumber);
                        break;
                    }


                }

            }

        }

        /// <summary>
        /// Another sub-method that tackles the variable addition part.
        /// </summary>
        /// <param name="line">The exact line of command which has variable assignment</param>
        public void variableassignment(String line)
        {
            
            string[] words = line.Split('=');
            for (int j = 0; j < words.Length; j++)
            {
                words[j] = words[j].Trim(); //trim every command
            }
            
            if (line.Contains('+'))
            {
               
                string[] operands = words[1].Split('+');
                string thevariable;
                int numericvalue=0;

                //check if the numeric value value is in right hand side of operator
                int no;

                if(int.TryParse(operands[1], out no))
                {
                    numericvalue = Convert.ToInt32(operands[1]);
                    thevariable = operands[0].Trim().ToLower();
                }
                else
                {
                    numericvalue = Convert.ToInt32(operands[0]);
                    thevariable = operands[1].Trim().ToLower();
                }

                
                int index = variablelist.IndexOf(thevariable);
                int k2 = Convert.ToInt32(variablevaluelist[index]);
                k2 = (k2 + numericvalue); //adding the value to the already existing value of variable.

                variablevaluelist.Insert(index, k2); //inserting the new value in the index of old value
          
                

            }

            else
            {
                /**
                 * Important part of the program where variables and values are added to their respective arraylists
                 */
                //before adding, check if there is + operator
                variablelist.Add(words[0]);//add the variable
                variablevaluelist.Add(words[1]);//add the value
            
            }
        }

        /// <summary>
        /// It is also a helper method that tackles the regular shape commands and executes it
        /// </summary>
        /// <param name="line">The exact line of command which tells to create a shape</param>
        public void shapescommand(String line)
        {
            String[] words = line.Split(' ');

            if (words[0].Trim().ToLower() == "circle")
            {
                bool isInt;
                int radius = 0;
                newshape = newsf.createShape("circle"); //Using shape factory to create the circle shape.
                isInt = words[1].All(char.IsDigit);
                if (isInt)
                {
                    radius = Int32.Parse(words[1]);
                }
                else
                {
                    int index = variablelist.IndexOf("radius");          //using the index of radius variable from the variable list
                    radius = Convert.ToInt32(variablevaluelist[index]);  //to actually send the value of radius
                }

                int[] arraytosend3 = { this.xaxis, this.yaxis, radius };



                newshape.set(defaultcolor, "false", arraytosend3); //then using the shape to set the values of the circle
                Graphics g = OutputWindow.CreateGraphics();
                newshape.draw(g);                                   //finally drawing the shape
                double tosubstract = radius / 3;

                this.xaxis = this.xaxis + radius - (int)tosubstract;
                this.yaxis = this.yaxis + radius - (int)tosubstract; //updating the xaxis and yaxis once the shape is drawn

            }
            else if (words[0].Trim().ToLower() == "rectangle")
            {
                bool isInt;
                int width = 0;
                int height = 0;

                newshape = newsf.createShape("rectangle");  //Using shape factory to create the rectangle shape.
                string[] parameters = words[1].Split(',');

                

                isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                if (isInt)
                {
                    try
                    {
                        width = Int32.Parse(parameters[0]); //using the index of width and height variable from the variable list
                        height = Int32.Parse(parameters[1]);
                    }
                    catch(Exception e)
                    {
                        MessageBox.Show("Do not keep space before parameters at line number:" + part2linenumber);
                    }
                }
                else
                {
                    int index = variablelist.IndexOf("width");
                    int index2 = variablelist.IndexOf("height");
                    width = Convert.ToInt32(variablevaluelist[index]);
                    height = Convert.ToInt32(variablevaluelist[index2]);
                   
                }



                int[] arraytosend3 = { this.xaxis, this.yaxis, width, height };
                newshape.set(defaultcolor, "false", arraytosend3);
                Graphics g = OutputWindow.CreateGraphics();
                newshape.draw(g);                               //finnally drawing the shape

                this.xaxis = this.xaxis + width;
                this.yaxis = this.yaxis + height; //updating the xaxis and yaxis once the shape is drawn

            }
            else if (words[0].Trim().ToLower() == "triangle")
            {
                bool isInt;
                int breadth = 0;
                int theight = 0;
                int perpendicular = 0;

                newshape = newsf.createShape("triangle");
                string[] parameters = words[1].Split(',');

               
                 isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit) && parameters[2].All(char.IsDigit));
                

                if (isInt)
                {
                    try
                    { 

                    breadth = Int32.Parse(parameters[0]);
                    theight = Int32.Parse(parameters[1]);
                    perpendicular = Int32.Parse(parameters[2]);
                    }
                    catch (Exception e)
                    {
                    MessageBox.Show("Do not keep space before parameters at line number:" + part2linenumber);
                    }
                }
                else
                {
                    int index = variablelist.IndexOf("breadth");
                    int index2 = variablelist.IndexOf("theight");
                    int index3 = variablelist.IndexOf("perpendicular");
                    breadth = Convert.ToInt32(variablevaluelist[index]);
                    theight = Convert.ToInt32(variablevaluelist[index2]);
                    perpendicular = Convert.ToInt32(variablevaluelist[index3]);
                
                }


                int[] arraytosend3 = { this.xaxis, this.yaxis, breadth, theight, perpendicular };
                newshape.set(defaultcolor, "false", arraytosend3);
                Graphics g = OutputWindow.CreateGraphics();
                newshape.draw(g);

                this.xaxis = breadth;
                this.yaxis = perpendicular;


            }
            else if (words[0].Trim().ToLower() == "drawto")
            {
                bool isInt;
                int point1 = 0;
                int point2 = 0;
                string[] parameters = words[1].Split(',');
                isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                if (isInt)
                {
                    try { 
                    point1 = Int32.Parse(parameters[0]);
                    point2 = Int32.Parse(parameters[1]);
                    }
                    catch (Exception e)
                    {
                    MessageBox.Show("Do not keep space before parameters at line number:" + part2linenumber);
                    }
            }
                else
                {
                    int index = variablelist.IndexOf("value1");
                    int index2 = variablelist.IndexOf("value2");
                    point1 = Convert.ToInt32(variablevaluelist[index]);
                    point2 = Convert.ToInt32(variablevaluelist[index2]);
                }


                DrawLine(point1, point2);
            }
            else if (words[0].Trim().ToLower() == "moveto")
            {
                bool isInt;
                int point1 = 0;
                int point2 = 0;
                string[] parameters = words[1].Split(',');
                isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                if (isInt)
                {
                    try { 
                    point1 = Int32.Parse(parameters[0]);
                    point2 = Int32.Parse(parameters[1]);
                    }
                    catch (Exception e)
                    {
                    MessageBox.Show("Do not keep space before parameters at line number:" + part2linenumber);
                    }
            }
                else
                {
                    int index = variablelist.IndexOf("value1");
                    int index2 = variablelist.IndexOf("value2");
                    point1 = Convert.ToInt32(variablevaluelist[index]);
                    point2 = Convert.ToInt32(variablevaluelist[index2]);
                }
                this.xaxis = point1;
                this.yaxis = point2;
            }

        }

        /// <summary>
        /// In this method, the if statement is processed, it first searches for endif line, and then executes all the statement inside the if statement
        /// </summary>
        public void checkifstatement()
        {
            
            //when 'if' is detected, first find the endif
            string commandlines = ProgramBox.Text;
            string[] eachline = commandlines.Split('\n');
            
            
            for (int i = 0; i < eachline.Length; i++) //all this for finding out the endif
            {
                iflineNumber = i + 1;
                eachline[i] = Regex.Replace(eachline[i], @"\s+", " ");
                string[] words = eachline[i].Split(' ');
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                    if (words[j] == "endif")
                    {
                        endiflinenumber = iflineNumber;
                        
                    }
                }
            }

            //now execute commands accordingly
            for (int k = part2linenumber + 1; k < endiflinenumber; k++)//again go through each command line inside the if/endif
            {
                
                eachline[k - 1] = Regex.Replace(eachline[k - 1], @"\s+", " ");
                
                string[] words = eachline[k - 1].Split(' ');

                if (words[0].Trim().ToLower() == "loop") //when loop command is found inside if
                {

                    bool isInt = words[1].All(char.IsDigit);
                    if (isInt)
                    {
                        looptime = Int32.Parse(words[1]);
                    }
                    else
                    {
                        int index = variablelist.IndexOf("repeat");
                        looptime = Convert.ToInt32(variablevaluelist[index]);
                    }

                    loopCommand(looptime); //again, the loopCommand is called from inside the ifcheck method
                }

                else if (words[0].Trim().ToLower() == "circle" && ifHasLoop==false) //when circle method is recognized 
                {
                      
                    bool isInt;
                    int radius = 0;
                    newshape = newsf.createShape("circle");
                    isInt = words[1].All(char.IsDigit);
                    if (isInt)
                    {
                        radius = Int32.Parse(words[1]);
                    }
                    else
                    {
                        int index = variablelist.IndexOf("radius");
                        radius = Convert.ToInt32(variablevaluelist[index]);
                    }

                    int[] arraytosend3 = { this.xaxis, this.yaxis, radius };



                    newshape.set(defaultcolor, "false", arraytosend3);
                    Graphics g = OutputWindow.CreateGraphics();
                    newshape.draw(g);
                    double tosubstract = radius / 3;

                    this.xaxis = this.xaxis + radius - (int)tosubstract;
                    this.yaxis = this.yaxis + radius - (int)tosubstract;

                }

                else if (words[0].Trim().ToLower() == "rectangle" && ifHasLoop == false)//when rectangle method is recognized
                {
                    bool isInt;
                    int width = 0;
                    int height = 0;

                    newshape = newsf.createShape("rectangle");
                    string[] parameters = words[1].Split(',');
                    isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                    if (isInt)
                    {

                        width = Int32.Parse(parameters[0]);
                        height = Int32.Parse(parameters[1]);
                    }
                    else
                    {
                        int index = variablelist.IndexOf("width");
                        int index2 = variablelist.IndexOf("height");
                        width = Convert.ToInt32(variablevaluelist[index]);
                        height = Convert.ToInt32(variablevaluelist[index2]);
                    }



                    int[] arraytosend3 = { this.xaxis, this.yaxis, width, height };
                    newshape.set(defaultcolor, "false", arraytosend3);
                    Graphics g = OutputWindow.CreateGraphics();
                    newshape.draw(g);

                    this.xaxis = this.xaxis + width;
                    this.yaxis = this.yaxis + height;

                }
                else if (words[0].Trim().ToLower() == "triangle" && ifHasLoop == false)//when triangle method is recognized
                {
                    bool isInt;
                    int breadth = 0;
                    int theight = 0;
                    int perpendicular = 0;

                    newshape = newsf.createShape("triangle");
                    string[] parameters = words[1].Split(',');
                    isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit) && parameters[2].All(char.IsDigit));
                    if (isInt)
                    {

                        breadth = Int32.Parse(parameters[0]);
                        theight = Int32.Parse(parameters[1]);
                        perpendicular = Int32.Parse(parameters[2]);
                    }
                    else
                    {
                        int index = variablelist.IndexOf("breadth");
                        int index2 = variablelist.IndexOf("theight");
                        int index3 = variablelist.IndexOf("perpendicular");
                        breadth = Convert.ToInt32(variablevaluelist[index]);
                        theight = Convert.ToInt32(variablevaluelist[index2]);
                        perpendicular = Convert.ToInt32(variablevaluelist[index3]);
                    }


                    int[] arraytosend3 = { this.xaxis, this.yaxis, breadth, theight, perpendicular };
                    newshape.set(defaultcolor, "false", arraytosend3);
                    Graphics g = OutputWindow.CreateGraphics();
                    newshape.draw(g);

                    this.xaxis = breadth;
                    this.yaxis = perpendicular;


                }
                else if (words[0].Trim().ToLower() == "drawto" && ifHasLoop == false)//when drawto command is recognized
                {
                    bool isInt;
                    int point1 = 0;
                    int point2 = 0;
                    string[] parameters = words[1].Split(',');
                    isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                    if (isInt)
                    {

                        point1 = Int32.Parse(parameters[0]);
                        point2 = Int32.Parse(parameters[1]);
                    }
                    else
                    {
                        int index = variablelist.IndexOf("value1");
                        int index2 = variablelist.IndexOf("value2");
                        point1 = Convert.ToInt32(variablevaluelist[index]);
                        point2 = Convert.ToInt32(variablevaluelist[index2]);
                    }


                    DrawLine(point1, point2);
                }
                else if (words[0].Trim().ToLower() == "moveto" && ifHasLoop == false)
                {
                    bool isInt;
                    int point1 = 0;
                    int point2 = 0;
                    string[] parameters = words[1].Split(',');
                    isInt = (parameters[0].All(char.IsDigit) && parameters[1].All(char.IsDigit));
                    if (isInt)
                    {

                        point1 = Int32.Parse(parameters[0]);
                        point2 = Int32.Parse(parameters[1]);
                    }
                    else
                    {
                        int index = variablelist.IndexOf("value1");
                        int index2 = variablelist.IndexOf("value2");
                        point1 = Convert.ToInt32(variablevaluelist[index]);
                        point2 = Convert.ToInt32(variablevaluelist[index2]);
                    }
                    this.xaxis = point1;
                    this.yaxis = point2;
                }
                else if (variable.Contains(words[0]))//for variable assignment inside if/endif
                {
                    variableassignment(eachline[k - 1]); //if the ifblock contains variable assignment, the command is sent to variable assignment method
                }
                
                else if (words[0].Trim().ToLower() == "method")
                {
                    MessageBox.Show("Method is not supported inside if at line number:" + part2linenumber);
                    break;
                }

            }


        }

        /// <summary>
        /// This sub method searches for the end statement of Method block, and stores each command in arraylist called storedCommands
        /// </summary>
        public void methodCommand()

        {
            //when 'method' is detected, first find the endmethod
            string commandlines = ProgramBox.Text;
            string[] eachline = commandlines.Split('\n');
            int endMethodlinenumber = 0;
            int alinenumber = 0;
            for (int i = 0; i < eachline.Length; i++) //all this for finding out the endmethod  
            {
                alinenumber = i + 1;
                eachline[i] = Regex.Replace(eachline[i], @"\s+", " ");
                string[] words = eachline[i].Split(' ');
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                    if (words[j] == "endmethod")
                    {
                        endMethodlinenumber = alinenumber;

                    }
                }
            }
            
            for (int k = part2linenumber + 1; k < endMethodlinenumber; k++)//again go through each command line inside method/endmethod
            {
                storedCommands.Add(eachline[k - 1]); //and storing the commands in an arraylist
              
            }
            
            }//end of method command

        /// <summary>
        /// In this method, all commands inside the simple/regular method is recognized and then sent to either variable assignment or in shape commands
        /// </summary>
        public void executeMethodCommand()
        {
            string commandline;
            //before executing the method, check if it is empty
            if (storedCommands.Count != 0)//checking if empty method is defined
            {
                for (int l = 0; l < storedCommands.Count; l++)
                {
                    commandline = (string)storedCommands[l];
                    //string words[] = commandline.Split(' ');
                    if (commandline.Contains('='))
                    {
                        variableassignment(commandline); //variable assignment inside method loop and ifs left
                    }
                    else if (commandline.Contains("loop") || commandline.Contains("if"))
                    {
                        
                        MessageBox.Show("method cannot support loops or ifs");
                        break;
                    }
                    else
                    {
                        shapescommand(commandline);//shape command inside method

                    }

                }
            }
            else
            {
                MessageBox.Show("Your method is empty!");
            }
        }

        /// <summary>
        /// Like the methodCommand, this method first searches for the closing command of the parameterized method and each command in the parameterized 
        /// is stored in the storedPCommand arraylist
        /// </summary>
        public void PmethodCommand()
        {

            //when 'method' is detected, first find the endmethod
            string commandlines = ProgramBox.Text;
            string[] eachline = commandlines.Split('\n');
            int endPMethodlinenumber = 0;
            int alinenumber = 0;
            for (int i = 0; i < eachline.Length; i++) //all this for finding out the endmethod  
            {
                alinenumber = i + 1;
                eachline[i] = Regex.Replace(eachline[i], @"\s+", " ");
                string[] words = eachline[i].Split(' ');
                for (int j = 0; j < words.Length; j++)
                {
                    words[j] = words[j].Trim();
                    if (words[j] == "endpmethod")
                    {
                        endPMethodlinenumber = alinenumber;

                    }
                }
            }
            
            for (int k = part2linenumber + 1; k < endPMethodlinenumber; k++)//again go through each command line inside method/endmethod
            {
                storedPCommands.Add(eachline[k - 1]);

            }
            
        } //for finding endpmethod

        /// <summary>
        /// This method is responsible for executing all the commands inside the parameterized method, uses the commands stored in the storedPCommands
        /// arraylist.
        /// </summary>
        /// <param name="arguments">The number of arguments sent when the method is called using the call mypmethod command</param>
        public void PexecuteMethodCommand(String arguments)
        {
            
            string[] var = arguments.Split('(');
            if (storedPCommands.Count != 0)//checking if empty method is defined
            {
                if (var.Length != 2)
                {
                    MessageBox.Show("Incorrect syntax for calling parameterized method"); //this means parameters are not sent
                }
                else
                {
                    var[1] = var[1].Replace(")", "");
                    string[] args = var[1].Split(',');
                    int argsNum = args.Length;

                    string commandline;
                    for (int l = 0; l < storedPCommands.Count; l++) //the order of parameters is most important
                    {
                        commandline = (string)storedPCommands[l]; //using the stored commands from parameterized method defination
                        string[] Aword = commandline.Split(' ');
                        if (Aword[0].Trim().ToLower() == "circle")
                        {
                            string radius = args[0];
                            string newcommand = "circle " + radius;

                            shapescommand(newcommand);
                        }
                        else if (Aword[0].Trim().ToLower() == "rectangle")
                        {
                            string width = args[0];
                            string height = args[1];
                            string newcommand = "rectangle " + width + "," + height;//molding the commands in a specific way

                            shapescommand(newcommand);
                        }
                        else if (Aword[0].Trim().ToLower() == "triangle")
                        {
                            string breadth = args[0];
                            string perpendicular = args[1];
                            string theight = args[2];
                            string newcommand = "triangle " + breadth + "," + perpendicular + "," + theight; //molding the commands in a specific way

                            shapescommand(newcommand);
                        }
                        else if (Aword[0].Trim().ToLower() == "drawto")
                        {
                            string value1 = args[0];
                            string value2 = args[1];
                            string newcommand = "drawto " + value1 + "," + value2; //molding the commands in a specific way
                            shapescommand(newcommand);
                        }
                        else if (Aword[0].Trim().ToLower() == "moveto")
                        {
                            string value1 = args[0];
                            string value2 = args[1];
                            string newcommand = "moveto " + value1 + "," + value2; //molding the commands in a specific way
                            shapescommand(newcommand);
                        }
                        
                        else
                        {
                            MessageBox.Show("Further commands are not support by paremetrized method");
                        }

                    }

                }
            }
            else
            {
                MessageBox.Show("Your parameterized method is empty!");
            }

        }

       
    }
}
